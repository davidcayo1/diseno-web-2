import AddTodo from "./components/add-todo.js";
import ModalEdit from './components/modal.js';
import Filters from "./components/filters.js";

class View {
  constructor() {
    this.model = null;
    this.table = document.getElementById('table');
    this.addTodoForm = new AddTodo();
    this.modal = new ModalEdit();
    // this.filters = new Filters();
    
    // hacemos una misma referencia al onClick con un arrow function así usando la funcion 'addTodo' para registrar el todo
    this.addTodoForm.onClick((nombres, apellidos, email, celular, fecha, hora, description ) => this.addTodo(nombres, apellidos, email, celular, fecha, hora, description));
    
    this.modal.onClick((id, values) => this.editTodo(id, values));
    // this.filters.onClick((filters) => this.filter(filters));
  }

  setModel(model) {
    this.model = model;
  }

  // mostrar los datos en la vista
  render() {
    const todos = this.model.getTodos();
    todos.forEach((todo) => this.createRow(todo));
  }


  // para la busqueda de filtros
 /*  filter(filters) {
    const { type, words } = filters;
    // no tomamos en cuenta el primer elemento row, es donde están los encabezados
    const [, ...rows] = this.table.getElementsByTagName('tr');

    // lógica para ocultar lo que no queremos en la busqueda
    for (const row of rows) {
      const [title, description, completed] = row.children;
      let shouldHide = false;

      if (words) {
        // las palabras que no coincidan esas palabras las vamos a ocultar.
        shouldHide = !title.innerText.includes(words) && !description.innerText.includes(words);
      }
      // si el completo el tipo
      const shouldBeComplete = type === 'completed';
      // agarramos el check si esta completed
      const isCompleted = completed.children[0].checked;
      
      // si el tipo no es 'all', y el shouldBeComplete no es checked
      if (type !== 'all' && shouldBeComplete !== isCompleted) {
        shouldHide = true;
      }

      // si es true
      if (shouldHide) {
        row.classList.add('d-none');
      } else {
        row.classList.remove('d-none');
      }
    }

  } */


  // para añadir un todo
  addTodo(nombres, apellidos, email, celular, fecha, hora, description ) {
    const todo = this.model.addTodo(nombres, apellidos, email, celular, fecha, hora, description);
    this.createRow(todo);
  }

  // para cambiar el toggle
  toggleCompleted(id) {
    this.model.toggleCompleted(id);
  }

  // para editar
  editTodo(id, values) {
    this.model.editTodo(id, values);
    const row = document.getElementById(id);
    row.children[0].innerText = values.nombres;
    row.children[1].innerText = values.apellidos;
    row.children[2].innerText = values.email;
    row.children[3].innerText = values.celular;
    row.children[4].innerText = values.fecha;
    row.children[5].innerText = values.hora;
    row.children[6].innerText = values.description;
    // row.children[4].children[0].checkbox = values.completed;
  }

  // para eliminar
  removeTodo(id) {
    this.model.removeTodo(id); // esto viene de el modelo
    document.getElementById(id).remove();
  }

  // Creamos la tabla donde se muestra los datos
  createRow(todo) {
    // insertamos una nueva fila
    const row = table.insertRow();

    row.setAttribute('id', todo.id);
    row.innerHTML = `
      <td>${todo.nombres}</td>
      <td>${todo.apellidos}</td>
      <td>${todo.email}</td>
      <td>${todo.celular}</td>
      <td>${todo.fecha}</td>
      <td>${todo.hora}</td>
      <td>${todo.description}</td>
      
      <td class="text-center">
      
      </td>
      <td class="text-right">
      
      </td>
    `;

    // creamos el checkbox manualmente
 /*    const checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkbox.classList.add('form-check-input');
    checkbox.checked = todo.completed;
    checkbox.onclick = () => this.toggleCompleted(todo.id);
    row.children[4].appendChild(checkbox); */


    // creamos el edit button
    const editBtn = document.createElement('button');
    editBtn.classList.add('btn', 'btn-primary', 'btn-sm', 'mb-1');
    editBtn.innerHTML = '<i class="fa fa-edit"></i>';
    editBtn.onclick = () => this.modal.setValues({
      id: todo.id,
      nombres: row.children[0].innerText,
      apellidos: row.children[1].innerText,
      email: row.children[2].innerText,
      celular: row.children[3].innerText,
      fecha: row.children[4].innerText,
      hora: row.children[5].innerText,
      description: row.children[6].innerText,
      /* fecha: row.children[2].innerText,
      hora: row.children[3].innerText,
      completed: row.children[4].children[0].checked, */
    });
    editBtn.setAttribute('data-bs-toggle', 'modal');
    editBtn.setAttribute('data-bs-target', '#modal');
    row.children[7].appendChild(editBtn);

    // creamos el remove button
    const removeBtn = document.createElement('button');
    removeBtn.classList.add('btn', 'btn-danger', 'btn-sm', 'mb-1');
    removeBtn.innerHTML = '<i class="fa fa-trash"></i>';
    removeBtn.onclick = () => {
      let message = confirm("¿Está seguro de eliminar el registro?")
      if (message) {
        this.removeTodo(todo.id);
      } else {
        return;
      }
    }
    row.children[8].appendChild(removeBtn);
  }
}

export default View;