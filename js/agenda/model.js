class Model {
  constructor() {
    this.view = null;
    this.todos = JSON.parse(localStorage.getItem('todos'));
  
    if (!this.todos || this.todos.length < 1) {
      this.todos = [];
      this.currentId = 1;
    } else {
      // añadimos al último todo (por eso el -1) y lo sumamos en +1
      this.currentId = this.todos[this.todos.length - 1].id + 1;
    }
  }
  
  // Para ponerlo en la vista
  setView(view) {
    this.view = view;
  }

  // Para guardar en el local storage
  save() {
    localStorage.setItem('todos', JSON.stringify(this.todos));
  }
  
  // Para mostrar los Todos
  getTodos() {
    // devolvemos una copia de nuestros todos
    return this.todos.map((todo) => ({...todo}));
  }

  // Para encontrar el id
  findTodo(id) {
    return this.todos.findIndex((todo) => todo.id === id);
  }

  // Para hacer el cambio de completed en el checkbox
  toggleCompleted(id) {
    const index = this.findTodo(id);

    // obtenemos el id en un array de index
    const todo = this.todos[index];
    // cambiamos el valor de el checkbox
    todo.completed = !todo.completed;
    this.save();
  }

  // Para editar los todos
  editTodo(id, values) {
    const index = this.findTodo(id);
    // asignamos los nuevos valores al index que obtenemos.
    Object.assign(this.todos[index], values);
    this.save();
  }

  // Para agregar los todos
  addTodo(nombres, apellidos, email, celular, fecha, hora, description) {

    const todo = {
      // para incrementar el id en cada registro
      id: this.currentId++,
      nombres,
      apellidos,
      email,
      celular,
      description,
      fecha,
      hora,
      /* fecha,
      hora,
      completed:false, */
    }

    // Metemos un todo en el array de todos
    this.todos.push(todo);

    this.save();
    return {...todo};
  }

  // Para eliminar un todo
  removeTodo(id) {
    const index = this.findTodo(id);
    // splice, le damos un indice (index) y a partir de alli que nos elimine un todo
    this.todos.splice(index, 1);
    this.save();
  }
}

export default Model;