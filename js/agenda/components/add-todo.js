import Alert from "./alert.js";

class AddTodo {
  constructor() {
    this.registro = document.getElementById("registro");
    this.btn = document.getElementById("add");
    this.nombres = document.getElementById("nombres");
    this.apellidos = document.getElementById("apellidos");
    this.email = document.getElementById("email");
    this.celular = document.getElementById("celular");
    this.fecha = document.getElementById("fecha");
    this.hora = document.getElementById("hora");
    this.description = document.getElementById("description");

    this.nuevoRegistro = document.getElementById("nuevoRegistro");

    this.validEmail = /^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/;

    // pasamos el nombre del id 'alert' de el html
    this.alert = new Alert('modal-registro');
    this.alertExito = new Alert('modal-exitoso');

    // Para agregar fecha y hora
    /* this.fechaHora = new Date();
    this.fecha = this.fechaHora.toLocaleDateString();
    this.hora = this.fechaHora.toLocaleTimeString(); */
  }

  onClick(callback) {

    this.btn.onclick = () => {

      if (this.nombres.value === '' || this.apellidos.value === '' || this.email.value === '' || this.celular.value === '' || this.fecha.value === '' || this.hora.value === '' || this.description.value === '') {
        
        
        this.alert.show('Todos los campos son requeridos');
        
        setTimeout(() => {
          this.alert.hide();
        }, 3000);
        return;

      }  
      // Para verificar el email
      else if ( !(this.validEmail.test(this.email.value))) {
        this.alert.show('El campo Email es invalido');
        
        setTimeout(() => {
          this.alert.hide();
        }, 3000);
        return;
      }
      
      else {
        // this.alert.hide();
        this.alertExito.showRegistrado("Registrado existosamente");

        setTimeout(() => {
          this.alertExito.hide();
        }, 3000);

        callback(
          this.nombres.value, 
          this.apellidos.value, 
          this.email.value, 
          this.celular.value, /* this.fecha, this.hora, */ 
          this.fecha.value, 
          this.hora.value, 
          this.description.value);

          // agregar clase aqui para remover el modal
          window.document.getElementById("exampleModal").classList.remove("show")
          

        // Para borrar los valores en las cajas de texto una vez ingresados
        this.registro.reset();
      }
    }
  }
}


export default AddTodo;