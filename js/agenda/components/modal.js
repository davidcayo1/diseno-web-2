import Alert from './alert.js';

 class ModalEdit {
  constructor() {
    this.nombres = document.getElementById('modal-nombre');
    this.apellidos = document.getElementById('modal-apellido');
    this.email = document.getElementById('modal-email');
    this.celular = document.getElementById('modal-celular');
    this.description = document.getElementById('modal-description');
    this.fecha = document.getElementById('modal-fecha');
    this.hora = document.getElementById('modal-hora');
    this.btn = document.getElementById('modal-btn');

    this.validEmail = /^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/;
    // this.completed = document.getElementById('modal-completed');
    this.alert = new Alert('modal-alert');
    this.todo = null;
  }

  // Para mostrar los datos en el form de editar
  setValues(todo) {
    this.todo = todo;
    this.nombres.value = todo.nombres;
    this.apellidos.value = todo.apellidos;
    this.email.value = todo.email;
    this.celular.value = todo.celular;
    this.fecha.value = todo.fecha;
    this.hora.value = todo.hora;
    this.description.value = todo.description;
    // this.completed.checked = todo.completed;
  }

  onClick(callback) {

    this.btn.onclick = () => {

      // Para verificar el email
      if ( !(this.validEmail.test(this.email.value))) {
        this.alert.show('El campo Email es invalido');
        
        setTimeout(() => {
          this.alert.hide();
        }, 3000);
        return;
      }


      if ( !this.nombres.value || !this.apellidos.value || !this.email.value || !this.celular.value || !this.fecha.value || !this.hora.value || !this.description.value) {
        this.alert.show('Todos los campos son requeridos');
        setTimeout(() => {
          this.alert.hide();
        }, 3000);
        return;
      }
      // Código de Jquey de bootstrap para obtener el modal.
      $('#modal').modal('toggle');


      callback(this.todo.id, {
        nombres: this.nombres.value,
        apellidos: this.apellidos.value,
        email: this.email.value,
        celular: this.celular.value,
        fecha: this.fecha.value,
        hora: this.hora.value,
        description: this.description.value,
        // completed: this.completed.checked,
      });
    }
  }
}

export default ModalEdit;