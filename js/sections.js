(()=> {
  
  // Variables

  const d = document;
  const aboutMe = d.getElementById('about-me'),
        studies = d.getElementById('studies'),
        experience = d.getElementById('experience'),
        projects = d.getElementById('projects'),
        contacts = d.getElementById('contacts'),
        diary = d.getElementById('diary');

  const linkAboutMe = d.getElementById('link_about-me'),
        linkStudies = d.getElementById('link_studies'),
        linkExperience = d.getElementById('link_experience'),
        linkProjects = d.getElementById('link_projects'),
        linkContacts = d.getElementById('link_contacts'),
        linkDiary = d.getElementById('link_diary')


  // Functions

  const defaultDisplay = () => {
    aboutMe.style.display = 'flex';
    studies.style.display = 'none';
    experience.style.display = 'none';
    projects.style.display = 'none';
    contacts.style.display = 'none';
    diary.style.display = 'none';
  }
  document.onload = defaultDisplay();

  
  // Show each section when we click in it

  linkAboutMe.addEventListener('click', () => {
    aboutMe.style.display = 'flex';
    studies.style.display = 'none';
    experience.style.display = 'none';
    projects.style.display = 'none';
    contacts.style.display = 'none';
    diary.style.display = "none";
  });

  linkStudies.addEventListener("click", () => {
    aboutMe.style.display = "none";
    studies.style.display = "flex";
    experience.style.display = "none";
    projects.style.display = "none";
    contacts.style.display = "none";
    diary.style.display = "none";
  });

  linkExperience.addEventListener("click", () => {
    aboutMe.style.display = "none";
    studies.style.display = "none";
    experience.style.display = "flex";
    projects.style.display = "none";
    contacts.style.display = "none";
    diary.style.display = "none";
  });

  linkProjects.addEventListener("click", () => {
    aboutMe.style.display = "none";
    studies.style.display = "none";
    experience.style.display = "none";
    projects.style.display = "flex";
    contacts.style.display = "none";
    diary.style.display = "none";
  });

  linkContacts.addEventListener("click", () => {
    aboutMe.style.display = "none";
    studies.style.display = "none";
    experience.style.display = "none";
    projects.style.display = "none";
    contacts.style.display = "flex";
    diary.style.display = "none";
  });

  linkDiary.addEventListener("click", () => {
    aboutMe.style.display = "none";
    studies.style.display = "none";
    experience.style.display = "none";
    projects.style.display = "none";
    contacts.style.display = "none";
    diary.style.display = "block";
  });


})();